export class ApiUrls {
  static GetAllTires = 'http://185.120.250.214:800/GetAllTires';
  static GetAllTblitems = `${ApiUrls.getBaseUrl()}/TblItemsApi`;
  static ImagesLocation = `${ApiUrls.getBaseUrl()}/pics/`;
  static GetAllCities = `${ApiUrls.getBaseUrl()}/CityApi`;
  static PurchaseSaveURL = `${ApiUrls.getBaseUrl()}/PurchaseApi/Save`;
  static PurchaseItemSaveAllURL = `${ApiUrls.getBaseUrl()}/PurchaseItemApi/SaveAll`;

  static getBaseUrl(){
    //return 'http://localhost:5010' ;
    return 'http://38.132.99.139:180';
  }
}
export function chunkArray(arr, n) {
  var chunkLength = Math.max(arr.length / n, 1);
  var chunks = [];
  for (var i = 0; i < n; i++) {
    if (chunkLength * (i + 1) <= arr.length) {
      chunks.push(arr.slice(chunkLength * i, chunkLength * (i + 1)));
    }
  }
  return chunks;
}
