import React from 'react';
import {Button, Image, Text, View} from 'react-native';
import {SingletonHolder} from './NavigateManagerScreen';

export default class HelloScreen extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {};
  }

  componentDidMount(): void {
    setTimeout(() => {
      SingletonHolder.navigator.navigate('VerifyRegisterScreen');
    }, 1000);
  }

  render() {
    return (
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'space-around',
          flexDirection: 'column',
            backgroundColor:'#ee9e37'
        }}>
        <Text>به همراه ماشین کاچار خوش آمدید</Text>
        <Image
          source={require('../assets/img/check.png')}
          style={{width: 20, height: 20}}
        />
      </View>
    );
  }
}
