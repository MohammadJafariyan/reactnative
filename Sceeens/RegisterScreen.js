import React from 'react';
import {SingletonHolder} from './NavigateManagerScreen';
import {Button, Text, TextInput, View} from 'react-native';
import {UserService} from '../Service/UserService';
import {TokenService} from '../Service/tokenService';
import {MyResponseType} from '../Service/Myglobal';

export default class RegisterScreen extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {enterCodeEnabled:false};
  }

  componentDidMount(): void {}

  render() {
    return (
      <View
        style={{
          flex: 1,

          flexDirection: 'column',
        }}>
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          {this.state.enterCodeEnabled == false && (
            <Text>جهت ورود لطف شماره تلفن خود را وارد نمایید</Text>
          )}

          {this.state.enterCodeEnabled == true && (
            <Text>کد ارسال شده به موبایل خود را وارد نمایید</Text>
          )}
        </View>
        <View
          style={{flex: 1, alignItems: 'center', justifyContent: 'flex-start'}}>
          {this.state.enterCodeEnabled == false && (
            <TextInput
              style={{
                height: 40,
                width: '80%',
                borderColor: '#ee882c',
                borderWidth: 1,
                textAlign: 'center',
              }}
              maxLength={11}
              keyboardType={'numeric'}
              placeholder="09-- --- ----"
              onChangeText={text => this.setState({text})}
              value={this.state.text}
            />
          )}
          {this.state.enterCodeEnabled && (
            <TextInput
              style={{
                height: 40,
                width: '60%',
                borderColor: '#ee882c',
                borderWidth: 1,
                textAlign: 'center',
              }}
              maxLength={4}
              keyboardType={'numeric'}
              placeholder="- - - -"
              onChangeText={code => this.setState({code: code})}
              value={this.state.code}
            />
          )}
        </View>
        <View style={{flex: 1}}>
          {this.state.enterCodeEnabled == false && (
            <Button title=" ► ادامه" onPress={() => this.newRegister()} />
          )}

          {this.state.enterCodeEnabled && (
            <Button title=" ► ورود" onPress={() => this.RegisterCode()} />
          )}
        </View>
      </View>
    );
  }

  newRegister() {
    if (!this.state.text) {
      alert(' لطفا شماره تلفن را درست وارد نمایید');
    }

    const c = new UserService();
    c.register(this.state.text).then(res => {
      if (res.Type == MyResponseType.Success) {
        const tokenService = new TokenService();
        tokenService.saveToken(res).then(st => {
          SingletonHolder.token = st;
        });
      } else {

        //alert(res.Message);
        this.setState({enterCodeEnabled: true});
      }
    });
  }

  RegisterCode() {
    if (!this.state.code) {
      alert('لطفا کد را درست وارد نمایید');
    }
    SingletonHolder.navigator.navigate('TblItemsListScreen');
  }
}
