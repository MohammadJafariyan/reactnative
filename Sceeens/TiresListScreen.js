import React from 'react';
import {
  Button,
  default as Colors,
  Image,
  Picker,
  SafeAreaView,
  ScrollView, StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {chunkArray} from '../MyGlobal/ApiUrls';
import TblItemService from '../Service/TblItemService';
import CityService from '../Service/CityService';
import {SingletonHolder} from './NavigateManagerScreen';

export default class TblItemsListScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      ltires: [],
      rtires: [],
      selectedItems: [],
    };
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  getAllTblItems() {
    console.log('--------------------------calling--------------------------');
    var s = new TblItemService();
    return s.getAll();
  }

  getAllCities() {
    let s = new CityService();
    return s.getAllCity();
  }

  componentDidMount() {
    // todo: در این تابع ، وب سرویس ها فراخوانی می شوند
    this._isMounted = true;

    // todo:لیست شهر ها
    this.getAllCities().then(res => {
      this.setState({cities: res});
    });

    console.log('this.getAllTblItems()');
    // todo:لیست آیتم ها بهمراه فروشندگان
    this.getAllTblItems().then(res => {
      //  this.setState({tblItems: res});
      console.log('this.getAllTblItems() res ', res);

      var chunks = chunkArray(res, 2);

      console.log('chunks============>', chunks);
      this.setState({
        ltires: chunks[0],
        rtires: chunks[1],
      });
    });

    /*
                this.getAllTblItems().then(result => {
                  console.log('this._isMounted', this._isMounted);
                  //  result.map(m => {
                  //    m.pic = ApiUrls.ImagesLocation + m.pic;
                  //    console.log(m.pic);

                  //    // do stuff

                  //    return m;
                  //  });
                  /!*
                       result=  chunkArray(result, 2)[0];
                       result=  chunkArray(result, 2)[0];*!/
                  console.log('result============>', result);
                  var chunks = chunkArray(result, 2);

                  console.log('chunks============>', chunks);
                  this.setState({
                    ltires: chunks[0],
                    rtires: chunks[1],
                  });
                });
            */
  }

  renbody() {
  return  <View
        style={{
          flex: 1,
          justifyContent: 'space-around',
          flexDirection: 'column',
        }}>
      <View
          style={{
            flex: 1,
            backgroundColor: '#dfffdb',
            justifyContent:'space-between',
            flexDirection:'row',
            alignItems:'center'
          }}>
        {/*// todo: اینجا dropdown*/}
        <Picker
            selectedValue={this.state.language}
            style={{height: 50, width: 100}}
            onValueChange={(itemValue, itemIndex) =>
                this.setState({language: itemValue})
            }>
          {this.state.cities && this.showCitiesItems()}
        </Picker>

          
          <Button title="بروز رسانی" onPress={()=>this.refreshComp()} />
          <Button title="صفحه اول" onPress={()=>this.goHelloScreen()} />
      </View>
      <View
          style={{
            flex: 9,
            backgroundColor: '#e0daff',
          }}>
        {/*todo:اینجا لیست اقلام و در داخل پارانتز نام فروشنده نمایش داده می شود*/}
        {(this.state.rtires ||
            this.state.ltires) &&
        this.showTireList(this.state.rtires, this.state.ltires)}
      </View>

      <View
          style={{
            flex: 1,
            backgroundColor: '#8dfcff',
          }}>
        {this.state.selectedItems && this.state.selectedItems.length > 0 && (
            <Button title="خرید" onPress={() => this.goBuyScreen()} />
        )}
      </View>
    </View>
  }

  render() {
    // todo:در این متد صفحه اصلی اسکرین نمایش داده می شود
    return (

       <>
         <StatusBar barStyle="dark-content" />
         <SafeAreaView>
           <ScrollView
               contentInsetAdjustmentBehavior="automatic"
               style={styles.scrollView}>
             {this.renbody()}
           </ScrollView>
         </SafeAreaView>
         </>
    );
  }

  showTireList(Rlist, Llist) {
    // todo: تابع نمایش دهنده ی لیست بصورت دو ستونی
    return Rlist.map((m, i) => {
      let l_item = Llist.length <= i ? null : Llist[i];
      let r_item = Rlist.length <= i ? null : Rlist[i];

      return (
        <View
          style={{flex: 1, justifyContent: 'flex-start', flexDirection: 'row'}}>
          <View
            style={{
              flex: 1,
              alignItems: 'stretch',
              height: 150,
            }}>
            {l_item && this.showTblItem(l_item)}
          </View>
          <View
            style={{
              flex: 1,
              alignItems: 'stretch',
              height: 150,
            }}>
            {r_item && this.showTblItem(r_item)}
          </View>
        </View>
      );
    });
  }

  showCitiesItems() {
    // todo: تابع نمایش دهنده شهر ها
    return this.state.cities.map((m, i) => {
      return <Picker.Item label={m.name} key={m.id} value={m.id} />;
    });
  }

  showTblItem(item) {
    return (
      <TouchableOpacity
        key={item.id}
        style={{
          flex: 1,
          flexDirection: 'row',
          borderWidth: 1,
        }}
        onPress={() => this.selectThisItem(item)}>
        <View style={{flex: 1}}>
          <Image
            source={require('../assets/img/index.jpg')}
            style={{width: 50, height: 50}}
          />
        </View>
        <View style={{flex: 9, flexDirection: 'column'}}>
          <Text style={{flex: 1}}>{item.name}</Text>
          <View style={{flex: 1, flexDirection: 'row', alignItems: 'stretch'}}>
            <Text style={{flex: 1}}>
              {' '}
              {item.partnet.description} ({item.partnet.name})
            </Text>
          </View>
          <View style={{flex: 1, flexDirection: 'row', alignItems: 'stretch'}}>
            {item.selected && (
              <Image
                source={require('../assets/img/check.png')}
                style={{width: 20, height: 20}}
              />
            )}

            <Text style={{flex: 1}}> {item.price} تومان </Text>
                    <Button title={item.partnet.name} onPress={() => this.TestNavigate()} />
          </View>
          <View style={{flex: 1, flexDirection: 'row', alignItems: 'stretch'}}>
            {item.partnet.behaviour === 1 && (
              <Text style={{flex: 1}}> متحرک</Text>
            )}
            {item.partnet.behaviour === 0 && (
              <Text style={{flex: 1}}> ثابت</Text>
            )}
          </View>
          </View>
      </TouchableOpacity>
    );
  }
    
  selectThisItem(item) {
    if (!item.selected) {
      item.selected = true;
    } else {
      item.selected = false;
    }
    let rtires = [...this.state.rtires];
    let ltires = [...this.state.ltires];

    // todo:اگر از این لیست باشد
    var finded = rtires.findIndex(r => r.id == item.id);
    if (finded === -1) {
      finded = ltires.findIndex(r => r.id == item.id);

      // todo: اگر نبود از لیست سمت چپ بگرد
      if (finded === -1) {
        throw Error('finded is null');
      }

      ltires[finded] = item;

      this.setState({ltires: ltires});
    } else {
      rtires[finded] = item;
      this.setState({rtires: rtires});
    }

    // todo : آپدیت انتخاب شده ها
    let selectedItems = [...this.state.selectedItems];

    if (!selectedItems) {
      selectedItems = [];
    }

    // todo: اگر انتخاب شده باشد
    if (item.selected) {
      selectedItems.push(item);
    } else {
      // todo : اگر انتخاب نشده باشد ان را حذف کن
      var i = selectedItems.findIndex(s => s.id == item.id);
      selectedItems.splice(i, 1);
    }
    this.setState({selectedItems: selectedItems});
  }

  goBuyScreen() {
    SingletonHolder.selectedItems=this.state.selectedItems;
    SingletonHolder.navigator.navigate('BuySuccessedScreen');
  }

  refreshComp() {
    this.componentDidMount();
  }

    goHelloScreen() {
        SingletonHolder.navigator.navigate('HelloScreen');
    }

    TestNavigate() {
        SingletonHolder.navigator.navigate('CarInfo')
    }
}

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});
