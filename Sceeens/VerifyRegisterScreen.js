import React from 'react';
import {View} from 'react-native';
import {AsyncStorage} from 'react-native';
import {SingletonHolder} from './NavigateManagerScreen';
import {TokenService} from '../Service/tokenService';
import {MyResponse, MyResponseType} from '../Service/Myglobal';

export default class VerifyRegisterScreen extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {};
  }

  _retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('token');
      if (value !== null) {
        console.log('token', value);
        // Our data is fetched successfully
        SingletonHolder.token = value;
        return value;
      } else {
        return null;
      }
    } catch (error) {
      console.log(error);
      // Error retrieving data
    }
  };

  componentDidMount(): void {
    // دیتای ذخیره شده در خود موبایل را برمیگرداند
    this._retrieveData().then(res => {
      if (res) {
        // به سرور می فرستد تا مطمئن شود که توکن درست کار می کند
        this._verifyTokenCall().then(res => {
          if (res.Type == MyResponseType.Fail) {
            alert(res.Message);
            this.redirectToRegister();
          } else {
            this.redirectToInsideApp();
          }
        });
      }else{
        this.redirectToRegister();
      }
    });
  }

  render() {
    return <View />;
  }

  _verifyTokenCall() {
    const s = new TokenService();
    return s.verifyTokenCall();
  }

  redirectToInsideApp() {
    SingletonHolder.navigator.navigate('TblItemsListScreen');
  }

  redirectToRegister() {
    SingletonHolder.navigator.navigate('RegisterScreen');
  }
}
