import React from 'react';
import {View} from 'react-native';
import TblItemsListScreen from './TiresListScreen';
import BuySuccessedScreen from '../Service/BuySuccessedScreen';
import AfterBuySuccessScreen from '../Service/afterBuySuccessScreen';
import RegisterScreen from './RegisterScreen';
import HelloScreen from './HelloScreen';
import VerifyRegisterScreen from './VerifyRegisterScreen';
import CarInfo from '../Zaman/CarInfo';

export const SingletonHolder = {};

export default class NavigateManagerScreen extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      currentScreen: 'VerifyRegisterScreen',
    };
    // this.navigate('TblItemsListScreen');
  }

  navigate(name) {
    this.setState({currentScreen: name});
  }

  render() {
    SingletonHolder.navigator = this;
    // todo:در این متد صفحه اصلی اسکرین نمایش داده می شود
    return (
      <View
        style={{
          flex: 1,
        }}>
        {this.renderCurrentRoute()}
      </View>
    );
  }

  renderCurrentRoute() {
    if (this.state.currentScreen == 'TblItemsListScreen') {
      return <TblItemsListScreen />;
    } else if (this.state.currentScreen == 'BuySuccessedScreen') {
      return <BuySuccessedScreen />;
    } else if (this.state.currentScreen == 'AfterBuySuccessScreen') {
      return <AfterBuySuccessScreen />;
    } else if (this.state.currentScreen == 'RegisterScreen') {
        return <RegisterScreen />;
    } else if (this.state.currentScreen == 'HelloScreen') {
      return <HelloScreen />;
    } else if (this.state.currentScreen == 'VerifyRegisterScreen') {
        return <VerifyRegisterScreen />;
    } else if (this.state.currentScreen == 'CarInfo') {
        return <CarInfo />
    } else {
      throw new Error('خطا در سیستم ، اسکرین داده شده یافت نشد');
    }


  }
}

export const Routes = [
  {name: 'TblItemsListScreen', comp: TblItemsListScreen},
  {name: 'BuySuccessedScreen', comp: BuySuccessedScreen},
  {name: 'AfterBuySuccessScreen', comp: AfterBuySuccessScreen},
  {name: 'RegisterScreen', comp: RegisterScreen},
  {name: 'HelloScreen', comp: HelloScreen},
  {name: 'VerifyRegisterScreen', comp: VerifyRegisterScreen },
  {name: 'CarInfo', comp: CarInfo},

];
