import React from 'react';
import {Button, Text, View} from 'react-native';
import {SingletonHolder} from '../Sceeens/NavigateManagerScreen';
import {PurchaseItemService, PurchaseService} from './purchaseService';
function buy() {

}
export default class BuySuccessedScreen extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      selectedItems: SingletonHolder.selectedItems,
    };
  }

  render() {
    return (
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          flexDirection: 'column',
        }}>
        <Text>تعداد انتخاب شده : {this.state.selectedItems.length}</Text>
        <Button title="خرید" onPress={() => this.buy()} />
      </View>
    );
  }

  buy() {
    var surchaseService = new PurchaseService();

    var purchase = {};
    var d = new Date();
    purchase.PurchaseDate = `${d.getFullYear()}-${d.getMonth()}-${d.getDay()}T${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}`;

    console.log('surchaseService.Save(purchase)============>', purchase);
    surchaseService
      .Save(purchase)
      .then(res => {
        console.log(
          'surchaseService\n' +
            '      .Save(purchase)\n' +
            '      .then(res => { \n',
          res,
        );
        if (res) {
          return res.json();
        } else {
          return res;
        }
      })
      .then(id => {
        console.log('.then(id =>============>', id);
        var selectedItems = this.GetSelectedItems(id);

        console.log(
          'purchaseItemService.SaveAll(selectedItems)============>',
          selectedItems,
        );

        var purchaseItemService = new PurchaseItemService();
        purchaseItemService
          .SaveAll(selectedItems)
          .then(res => {
            SingletonHolder.selectedItems = this.state.selectedItems;
            SingletonHolder.navigator.navigate('AfterBuySuccessScreen');
          })
          .catch(e => {
            console.log('  .catch(e => {', e);
            alert('خطایی رخ داد');
            /*   alert(e);*!/*/
          });
      })
      .catch(e => {
        console.log('  .catch(e => {', e);
        alert('خطایی رخ داد');
        /*   alert(e);*!/*/
      });
  }

  GetSelectedItems(id) {
    var arr = [];
    for (let i = 0; i < this.state.selectedItems.length; i++) {
      var itemToPurchase = {};
      itemToPurchase.TblItemId = this.state.selectedItems[i].id;
      itemToPurchase.PurchaseId = id;
      arr.push(itemToPurchase);
    }
    return arr;
  }
}
