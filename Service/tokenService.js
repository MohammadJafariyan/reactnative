import {MyResponse, MyResponseType} from './Myglobal';
import {AsyncStorage} from 'react-native';

export class TokenService {
  verifyTokenCall(): Promise<MyResponse<any>> {
    const c = new MyResponse();
    c.Type = MyResponseType.Fail;
    return Promise.all(c);
  }

  saveToken(res): Promise<any> {
    return AsyncStorage.setItem('token', res);
  }
}
