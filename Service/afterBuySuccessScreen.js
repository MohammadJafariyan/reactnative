import React from 'react';
import {Button, Text, View} from 'react-native';
import {SingletonHolder} from '../Sceeens/NavigateManagerScreen';

export default class AfterBuySuccessScreen extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      selectedItems: SingletonHolder.selectedItems,
    };
  }

  render() {
    return (
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'space-around',
          flexDirection: 'column',
        }}>
          <Button title="بازگشت" onPress={() => this.backToHome()} />

        <Text style={{color: '#ff0907'}}>
          تعداد خرید شده : {this.state.selectedItems.length}
        </Text>

      </View>
    );
  }

  backToHome() {
    SingletonHolder.navigator.navigate('TblItemsListScreen');
  }
}
