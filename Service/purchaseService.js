import {ApiUrls} from '../MyGlobal/ApiUrls';

export class PurchaseService {
  Save(obj) {
    return fetch(ApiUrls.PurchaseSaveURL, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(obj),
    })
        ;
  }
}

export class PurchaseItemService {
  SaveAll(obj) {
    return fetch(ApiUrls.PurchaseItemSaveAllURL, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(obj),
    })
        ;
  }
}
