export class MyResponse<T> {
  Type: MyResponseType;
  Single: T;
  List: T[];
  Message: string;
}

export const MyResponseType = {
  Success: 2,
  Fail: 3,
};
