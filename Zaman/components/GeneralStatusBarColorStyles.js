import {
    StyleSheet,
    Platform,
    StatusBar
} from 'react-native';
const STATUSBAR_HEIGHT = Platform.OS === "android" ? StatusBar.currentHeight : 0
export default StyleSheet.create({
    statusBar: {
        paddingTop: STATUSBAR_HEIGHT
    }
});